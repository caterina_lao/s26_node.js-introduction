const http = require('http');

const PORT = 3000;

http.createServer(function(request, response){
	
	if(request.url == '/login'){
	response.writeHead(200,{'Content-Type': 'text/plain'})
	response.end('Welcome to the login page')
	} else {
	response.writeHead(404,{'Content-Type': 'text/plain'})
	response.end('Error: Page Not Found')
	}

}).listen(PORT);

console.log(`Server is running at localhost: ${PORT}.`);