//Routing is the way which we handle our clients request based on their endpoints.
const http = require('http');

const PORT = 4000;

http.createServer(function(request, response){
	
	if(request.url == '/homepage'){
	response.writeHead(200,{'Content-Type': 'text/plain'})
	response.end('This is the homepage')
	} else if(request.url == '/register'){
		response.writeHead(200,{'Content-Type': 'text/plain'})
		response.end('This is our Register Page')
	}
	else {
	//This will be our response if an endpoint passed in the client's request is not recognized ot there is no designated route for this endpoint.
	response.writeHead(404,{'Content-Type': 'text/plain'})
	response.end('Error: Page Not Found')
	}

}).listen(PORT);

console.log(`Server is running at localhost: ${PORT}.`);